%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: PlayerHandMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Box2
    m_Weight: 0
  - m_Path: Box2/BoxFlap
    m_Weight: 0
  - m_Path: Box2/BoxFlap/joint3
    m_Weight: 0
  - m_Path: Box2/Spring
    m_Weight: 1
  - m_Path: Box2/Spring/joint22
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/IndexFinger
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/IndexFinger/joint10
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/IndexFinger/joint10/joint11
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/IndexFinger/joint10/joint11/joint12
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/MiddleFinger
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/MiddleFinger/joint14
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/MiddleFinger/joint14/joint15
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/MiddleFinger/joint14/joint15/joint16
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/PinkyFinger
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/PinkyFinger/joint18
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/PinkyFinger/joint18/joint19
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/PinkyFinger/joint18/joint19/joint20
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/Thumb
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/Thumb/joint6
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/Thumb/joint6/joint7
    m_Weight: 1
  - m_Path: Box2/Spring/joint22/Hand 1/Thumb/joint6/joint7/joint8
    m_Weight: 1
  - m_Path: group2
    m_Weight: 0
  - m_Path: group2/Box
    m_Weight: 0
  - m_Path: group2/BoxCover
    m_Weight: 0
  - m_Path: group2/Hand
    m_Weight: 0
  - m_Path: group2/Spring 1
    m_Weight: 0
