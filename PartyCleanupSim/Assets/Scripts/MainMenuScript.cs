﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

	public GameObject[] creditsTexts;

	bool showingCredits;

    public void StartButton() {
        SceneManager.LoadScene("House");
    }

	public void CreditsButton(){
		if (showingCredits) {
			creditsTexts [0].SetActive (false);
			for (int i = 1; i < creditsTexts.Length; i++) {
				creditsTexts [i].SetActive (true);
			}
			showingCredits = false;

		} else {
			creditsTexts [0].SetActive (true);
			for (int i = 1; i < creditsTexts.Length; i++) {
				creditsTexts [i].SetActive (false);
			}
			showingCredits = true;
		}
	}

    public void ExitButton() {
        Application.Quit();
    }
}
