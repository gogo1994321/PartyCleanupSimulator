﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
/*
 * Place movement of player here. Input is handled inside PlayerControl!
 */
public class PlayerMove : MonoBehaviour
{

    public float speed = 10.0f;
    public float gravity = 10.0f;
    public float maxVelocityChange = 10.0f;
    public float jumpHeight = 2.0f;
    public float walkJumpHeight = 1.0f;
    public ForceMode forceMode;
    public bool rigidbodyCanRotate; //This was originally here so we could roll down the stairs maybe. Isn't really used
    public AudioClip jumpSound;
    public Animator anim;
    public float longIdleTimeout;   //How much time has to pass in order to enter the longer idle

    [HideInInspector]
    public bool isHoldingSomething; //So we can play correct holding animation

    private bool grounded = false;
    private Rigidbody rigidbody;
    private float originalSpeed;
    private float timeSinceLastLongIdle;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.freezeRotation = !rigidbodyCanRotate;
        rigidbody.useGravity = false;
        originalSpeed = speed;
        StartCoroutine(TranslateToSimpleIdle(2f));  //Used for the start-scene transition
    }

    public void RotatePlayer()
    {

        //Rotate player
        Quaternion rot = Camera.main.transform.rotation;
        rot.eulerAngles = new Vector3(0, rot.eulerAngles.y, 0);
        transform.rotation = rot;
    }

    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position,-Vector3.up, out hit))   //Are we on the ground?
        {
            if(hit.distance <= 0.1f)
                grounded = true;
        }

        //These are used to detect and trigger longer idle
        if (!Input.anyKey && timeSinceLastLongIdle > longIdleTimeout && !isHoldingSomething)
        {
            anim.SetBool("longIdle",true);
            timeSinceLastLongIdle = 0;
        }
        else 
        {
            if(!anim.GetBool("longIdle"))
                timeSinceLastLongIdle += Time.deltaTime;
           
            if (Input.anyKey)
                anim.SetBool("longIdle", false);
        }

        //Play correct holding anim
        anim.SetBool("dragging", isHoldingSomething);

    }

    public IEnumerator Move(Vector3 dir, bool jump)
    {
        
            // Calculate how fast we should be moving
            dir *= speed;
            anim.SetFloat("horizontalVel", dir.magnitude);

        if (dir.magnitude > 0) {    //This is here only for the very first jump to appear as if the animation is actually doing something
            yield return new WaitForSeconds(.23f);
        }
                
            // Apply a force that attempts to reach our target velocity
            Vector3 velocity = rigidbody.velocity;
            Vector3 velocityChange = (dir - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;
            
            
        rigidbody.AddForce(velocityChange, forceMode);

        if (grounded)
        {
            anim.SetTrigger("landing");
            anim.SetBool("jumping", false);
            AudioSource source = GetComponent<AudioSource>();
            // Jump
            if (jump && Input.GetButton("Jump"))
            {
                rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(jumpHeight), velocity.z);
                anim.SetBool("jumping", true);

                if(!source.isPlaying)
                    source.PlayOneShot(jumpSound);
            }
            if (dir.magnitude > 0)
            {
                rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(walkJumpHeight), velocity.z);   //For autojump
                anim.SetBool("jumping", true);

                if (!source.isPlaying)
                    source.PlayOneShot(jumpSound);
            }
        }

        // We apply gravity manually for more tuning control
        rigidbody.AddForce(new Vector3(0, -gravity * rigidbody.mass, 0));
        grounded = false;

    }

    float CalculateJumpVerticalSpeed(float localJumpHeight)
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * localJumpHeight * gravity);
    }


    public void PlayerReachedMaxDragDistance(bool reachedMax, Vector3 dir)
    { //Check if the player is still within range of the dragged object, if not, add a rebounding force
        if (reachedMax)
            rigidbody.AddForce(dir, ForceMode.Impulse);
    }

    IEnumerator TranslateToSimpleIdle(float wait) {
        anim.SetBool("longIdle", true);
        yield return new WaitForSeconds(wait);
        anim.SetBool("longIdle", false);
    }
}
