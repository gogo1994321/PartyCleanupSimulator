using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/*
 * Place all controlling of the player inside here! However not the execution of said control.
*/
public class PlayerControl : MonoBehaviour
{
    public bool canJump = true;
    public float maxInteractDistance = 100.0f;
    public Transform handPosition;
    [Tooltip("How fast can we rotate a tool in our hands")]
    public float rotationSpeed = 10f;
    public bool invertItemRotation;
    [Space]
    [Tooltip("What's the max distance we can be away from a dragged object before rebounding")]
    public float maxDragDistance;
    [Tooltip("The amount of force to apply to the player when we reached maxDragDistance")]
    public float reboundForce;

    public float throwForce;    //How hard can we throw a picked-up rigidbody?

    private PlayerMove playerMove; // Reference to the player controller.

    private Vector3 move;
    // the world-relative desired move direction, calculated from the camForward and user input.

    private Transform cam; // A reference to the main camera in the scenes transform
    private Vector3 camForward; // The current forward direction of the camera
    private bool jump; // whether the jump button is currently pressed

    private GameObject currentItem;   //The current tool we picked up if we have any (broom, mop etc.)
    private DragRigidbody dragScript;

    private int invert; //Small value to flip rotation of item while holding Q

    private List<bool> toolColliderStateList = new List<bool>();    //Needed to restore correct enabled-state to colliders after rotating tool

	public bool playerFrozen;

    private void Awake()
    {
        // Set up the reference.
        playerMove = GetComponent<PlayerMove>();
        dragScript = GetComponent<DragRigidbody>();

        // get the transform of the main camera
        if (Camera.main != null)
        {
            cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Ball needs a Camera tagged \"MainCamera\", for camera-relative controls.");
            // we use world-relative controls in this case, which may not be what the user wants, but hey, we warned them!
        }

        invert = invertItemRotation ? -1 : 1;


    }


    private void Update()
    {
		if (playerFrozen)
			return;


        #region Movement
        // Get the axis and jump input.

        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        jump = CrossPlatformInputManager.GetButton("Jump");

        // calculate move direction
        if (cam != null)
        {
            // calculate camera relative direction to move:
            camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
            move = (v * camForward + h * cam.right).normalized;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            move = (v * Vector3.forward + h * Vector3.right).normalized;
        }
        //Rotate only if we are moving, otherwise free up camera
        if (h != 0 || v != 0)
        {
            playerMove.RotatePlayer();
        }

        #endregion

        #region Using-or-Dragging
        //Using / Dragging item
        if (CrossPlatformInputManager.GetButtonDown("LMB"))
        {
            RaycastHit hit;
            if (!Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, maxInteractDistance))  //Is anything within actual interact distance?
                return;
            if (currentItem == null)    //We don't have anything in the Hand atm
            {

                if (hit.transform.GetComponentsInChildren<IInteractable>().Length != 0)
                {    //Mainly so we are able to use the sound play on interaction
                    foreach (IInteractable interactable in hit.transform.GetComponentsInChildren<IInteractable>())
                    {
                        interactable.Interact();
                    }
                }
                currentItem = hit.transform.gameObject; //Make sure to have the reference to the currently used item
                if (currentItem.GetComponent<ToolItem>())
                {    //If the item is a tool, teleport it to the hand and make it kinematic
                    currentItem.SetParentAtPosZero(handPosition);
                    currentItem.GetComponent<Rigidbody>().isKinematic = true;
                }
                else if (hit.rigidbody && !hit.rigidbody.isKinematic)   //If it's something draggable, drag it
                {
                    dragScript.Drag(hit, true);
                }
                playerMove.isHoldingSomething = true;   //This is only for the hand animation atm!
            }
            else
            {
                if (currentItem.GetComponentsInChildren<IInteractable>().Length != 0)
                {    //Mainly so we are able to use the sound play on interaction
                    foreach (IInteractable interactable in currentItem.GetComponentsInChildren<IInteractable>())
                    {
                        interactable.Interact();
                    }
                }
                ReleaseCurrentItem();
                currentItem = null;
            }
        }

        /*if (CrossPlatformInputManager.GetButtonUp("LMB")) {   //Change to using another Key to actually use the tool!
            if (currentItem != null)
            {
                currentItem.GetComponent<ToolItem>().UseTool(false);
            }
        }*/

        if (CrossPlatformInputManager.GetButtonDown("RMB")) //Either let go of the tool we are holding, or throw the dragged rigidbody
        {
            ReleaseCurrentItem();
			if(currentItem!=null)
				currentItem.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * throwForce, ForceMode.Impulse);
            
			currentItem = null;
        }
        #endregion

        #region Rotating Item
        //Rotating Item



        if (CrossPlatformInputManager.GetButton("ObjectRotate"))    //Rotate the item in hand around the global Y axis and the camera's forward axis. This part could be possibly shortened with a temp GO
        {
            float rot = rotationSpeed * Time.deltaTime;
			if (currentItem != null && currentItem.GetComponent<ToolItem>() != null)
            {
                currentItem.transform.Rotate(Camera.main.transform.right, rot * CrossPlatformInputManager.GetAxis("Mouse Y") * invert, Space.World);
                currentItem.transform.Rotate(Vector3.up, rot * CrossPlatformInputManager.GetAxis("Mouse X") * invert, Space.World);
            }
            else if (dragScript.isDragging())
            {
				dragScript.usedJoint().gameObject.transform.Rotate(Camera.main.transform.right, rot * CrossPlatformInputManager.GetAxis("Mouse Y") * invert, Space.World);
				dragScript.usedJoint().gameObject.transform.Rotate(Vector3.up, rot * CrossPlatformInputManager.GetAxis("Mouse X") * invert, Space.World);
            }
        }

        //So we don't push the player around if he accidentaly rotates the item into himself
		if (CrossPlatformInputManager.GetButtonDown("ObjectRotate"))
        {
            EnableCollidersOnItem(false);

        }
		if (CrossPlatformInputManager.GetButtonUp("ObjectRotate") || CrossPlatformInputManager.GetButtonUp("LMB"))
        {
            EnableCollidersOnItem(true);

        }
        #endregion

		if (dragScript.usedJoint () == null) {
			playerMove.isHoldingSomething = false;
		}
    }

    void ReleaseCurrentItem()
    {
		if (currentItem != null) {
			//If we are holding anything, and pressed LMB again, then make sure to release it correctly
			if (currentItem.GetComponent<ToolItem> ()) {
				currentItem.transform.SetParent (null);
				currentItem.GetComponent<Rigidbody> ().isKinematic = false;
				toolColliderStateList.Clear ();
			} else {
				dragScript.ForceRelease ();
			}
			playerMove.isHoldingSomething = false;
		}
    }

    void EnableCollidersOnItem(bool enable)
    {
        if (currentItem != null)
        {
            if (currentItem.GetComponent<ToolItem>())
            {
                Collider[] colliders = currentItem.transform.GetComponentsInChildren<Collider>(true);
                if (colliders.Length > 0)
                {
                    if (enable)
                    {
                        for (int i = 0; i < toolColliderStateList.Count; i++)
                        {
                            colliders[i].enabled = toolColliderStateList[i];
                        }
                        
                    }
                    else
                    {
                        for (int i = 0; i < colliders.Length; i++)
                        {
                            toolColliderStateList.Clear();
                            toolColliderStateList.Add(colliders[i].enabled);
                            Debug.Log(colliders.Length+" = "+toolColliderStateList.Count);
                            colliders[i].enabled = false;
                        }
                    }
                }
            }
			else if(dragScript.isDragging())
            {
                foreach (Collider child in dragScript.draggedObject().transform.GetComponentsInChildren<Collider>())
                {
                    child.enabled = enable;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (currentItem == null && dragScript.isDragging())
        {
            Vector3 directionToObject = dragScript.draggedObject().transform.position - transform.position;
            playerMove.PlayerReachedMaxDragDistance(dragScript.isDragging() && directionToObject.magnitude > maxDragDistance, directionToObject.normalized * reboundForce);
        }
        // Call the Move function of the ball controller
        StartCoroutine(playerMove.Move(move, canJump && jump));
        jump = false;
    }
}
