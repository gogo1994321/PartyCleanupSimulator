﻿using UnityEngine;
using System.Collections;

/*
 * Drag any available rigidbody that the player focuses on and is inside the range. This script is almost identical to the one in the Standard Assets, but instead of a Spring Joint we use a Fixed one.
 */

public class DragRigidbody : MonoBehaviour {

    public bool attachToCenterOfMass = false;

    private GameObject dragger;
    private FixedJoint fixedJoint;

    //For addind velocity to small object after letting go
    private Vector3 prevObjPos;
    private float posSampleInterval = 0.25f;
    [SerializeField]
    private float throwForceMultiplier = 2f;
    [SerializeField]
    [Tooltip("Amount of stress that causes the joint to break")]
    private float maxStressBeforeBreak;

    private float dragDistanceModifier = 1; //Used so we can drag the item closer to us with MMB
    private bool toggleDrag;    //Tracks the toggle of the grabbing/dropping of items

    public void Drag(RaycastHit hit, bool doDrag)
    {
        if (!dragger)
        {
            GameObject go = new GameObject("Rigidbody dragger");
            Rigidbody body = go.AddComponent<Rigidbody>();
            body.isKinematic = true;
            dragger = go;
        }

        if(!fixedJoint)
        {
            fixedJoint = dragger.AddComponent<FixedJoint>();
        }
       
        fixedJoint.transform.position = hit.point;
        fixedJoint.transform.rotation = Camera.main.transform.rotation;
        if(attachToCenterOfMass)
        {
            Vector3 anchor = transform.TransformDirection(hit.rigidbody.centerOfMass) + hit.rigidbody.transform.position;
            anchor = fixedJoint.transform.InverseTransformPoint(anchor);
            fixedJoint.anchor = anchor;
        }
        else
        {
            fixedJoint.anchor = Vector3.zero;
        }
        fixedJoint.connectedBody = hit.rigidbody;
        fixedJoint.breakForce = maxStressBeforeBreak;
        fixedJoint.breakTorque = maxStressBeforeBreak;
        //Debug.Log("Rigidbody:"+hit.rigidbody.name+" jointConBodyName: "+springJoint.connectedBody.name);
        toggleDrag = doDrag;

        StartCoroutine(DragObject(hit.distance*dragDistanceModifier));
    }
   
    IEnumerator DragObject(float distance)
    {
        float timer = 0;
        while(toggleDrag && fixedJoint && fixedJoint.connectedBody)
        {

            dragDistanceModifier += Input.GetAxis("Mouse ScrollWheel");
            dragDistanceModifier = dragDistanceModifier > 1 ? 1 : dragDistanceModifier;
            dragDistanceModifier = dragDistanceModifier < 0.6f ? 0.6f : dragDistanceModifier;

            Ray ray = new Ray(Camera.main.transform.position,Camera.main.transform.forward);
            fixedJoint.transform.position = ray.GetPoint(distance*dragDistanceModifier);
            if (timer < posSampleInterval)
            {
                timer++;
            }
            else {
                prevObjPos = fixedJoint.connectedBody.position;
                timer = 0;
            }
            yield return null;
        }
       
        if(fixedJoint && fixedJoint.connectedBody)
        { 
            ForceRelease();
        }
    }

    public bool isDragging() {
        return fixedJoint && fixedJoint.connectedBody != null && toggleDrag;
    }

    public GameObject draggedObject() {
		return fixedJoint!=null ? fixedJoint.connectedBody.gameObject : null;
    }

    public FixedJoint usedJoint() {
        return fixedJoint;
    }

    public void ForceRelease() {
        dragDistanceModifier = 1;
		if (fixedJoint != null && fixedJoint.connectedBody != null) {
			Vector3 newPos = fixedJoint.connectedBody.position;
			Rigidbody GO = fixedJoint.connectedBody;
			fixedJoint.connectedBody = null;
        
			//Add velocity to rigidbody since we are letting it go
			GO.velocity = ((newPos - prevObjPos) / posSampleInterval) * throwForceMultiplier;
		}
		toggleDrag = false;
    }
}
