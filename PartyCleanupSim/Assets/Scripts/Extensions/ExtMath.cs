using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static  class ExtMath{

	/// <summary>
    /// Lerp a value from one range to another
    /// </summary>
    /// <example>
    /// Lerp(5, 0, 10, 0, 1) == 0.5
    /// Lerp(5, 0, 10, 0, 1)
    /// </example>
	public static float Lerp(float value, float inStart, float inEnd, float outStart, float outEnd)
    {
        float normed = (value - inStart) / (inEnd - inStart);
        return outStart + (normed * (outEnd - outStart));
    }
	
	
	
	public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}
