﻿using UnityEngine;
using System.Collections;

public static class ExtSprite
{
		/// <summary>
		/// Sprite Pivot Enum
		/// </summary>
		public enum SpritePivot
		{
				TopLeft,
				Top,
				TopRight,
				Left,
				Center,
				Right,
				BottomLeft,
				Bottom,
				BottomRight
		}

		/// <summary>
		/// Gets the pivot position.
		/// </summary>
		public static Vector2 GetPivotPosition (ExtSprite.SpritePivot pivot)
		{
				switch (pivot) {
				case SpritePivot.TopLeft:
						return new Vector2 (0, 1);
						//break;
				case SpritePivot.Top:
						return new Vector2 (0.5f, 1);
						//break;
				case SpritePivot.TopRight:
						return new Vector2 (1, 1);
						//break;
				case SpritePivot.Left:
						return new Vector2 (0, .5f);
						//break;
				case SpritePivot.Center:
						return new Vector2 (0.5f, .5f);
						//break;
				case SpritePivot.Right:
						return new Vector2 (1, .5f);
						//break;
				case SpritePivot.BottomLeft:
						return new Vector2 (0, 0);
						//break;
				case SpritePivot.Bottom:
						return new Vector2 (0.5f, 0);
						//break;
				case SpritePivot.BottomRight:
						return new Vector2 (1, 0);
						//break;
				}
				return Vector2.zero;
		}

		/// <summary>
		/// Creates a sprite from texture.
		/// </summary>
		public static Sprite CreateSpriteFromTexture (Texture2D tex, Vector2 pivot, float pixelToUnits)
		{
				return (Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), pivot, pixelToUnits));
		}

		/// <summary>
		/// Creates a sprite from texture.
		/// </summary>	
		public static Sprite CreateSpriteFromTexture (Texture2D tex, ExtSprite.SpritePivot pivot, float pixelToUnits)
		{
				return (Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), ExtSprite.GetPivotPosition (pivot), pixelToUnits));
		}

		/// <summary>
		/// Creates a sprite from texture.
		/// </summary>
		public static Sprite CreateSpriteFromTexture (Texture2D tex, Vector2 pivot, float pixelToUnits, Rect rect)
		{
				return (Sprite.Create (tex, rect, pivot, pixelToUnits));
		}

		/// <summary>
		/// Creates a sprite from texture.
		/// </summary>	
		public static Sprite CreateSpriteFromTexture (Texture2D tex, ExtSprite.SpritePivot pivot, float pixelToUnits, Rect rect)
		{
				return (Sprite.Create (tex, rect, ExtSprite.GetPivotPosition (pivot), pixelToUnits));
		}
		

}
