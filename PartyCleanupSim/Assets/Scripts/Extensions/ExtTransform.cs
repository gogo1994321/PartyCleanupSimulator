using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ExtTransform
{
    //Get Child
    public static Transform GetChild(this Transform _tr, string name)
    {
        
        Component[] transforms = _tr.GetComponentsInChildren(typeof(Transform), true);
        foreach (Transform transform in transforms)
        {
            if (transform.gameObject.name == name)
            {
                return transform;
            }
        }
        return null;
    }
    
    
    //Get children
    public static List<Transform> GetChildren(this Transform _tr)
    {
        List<Transform> children = new List<Transform>();
        foreach (Transform child in _tr)
        {

            children.Add(child);

        }
        return children;
    }
    
    //Get All children
    public static List<Transform>  GetAllChildren(this Transform _tr)
    {

        List<Transform> children = _tr.GetChildren();
    
        for (var i = 0; i < children.Count; i++)
        {

            List<Transform> moreChildren = children [i].GetChildren();

            for (var j = 0; j < moreChildren.Count; j++)
            {

                children.Add(moreChildren [j]);

            }

        }
        return children;

    }
    
    public static Transform[] FindChildrenWithName(this Transform _tr, string name, bool contains = false)
    {
        List<Transform> children = _tr.GetAllChildren();
        List<Transform> results = new List<Transform>();
        for (var i = 0; i < children.Count; i++)
        {
            if (!contains && children [i].name == name)
                results.Add(children [i]);
            if (contains && children [i].name.ToLower().Contains(name.ToLower()))
                results.Add(children [i]);
        }
        
        return results.ToArray();
    }
    
    
    //Set Position X
    public static void SetX(this Transform _tr, float value)
    {
        Vector3 newPosition = new Vector3(value, _tr.position.y, _tr.position.z);
        _tr.position = newPosition;
    }
    
    //Set Position Y
    public static void SetY(this Transform _tr, float value)
    {
        Vector3 newPosition = new Vector3(_tr.position.x, value, _tr.position.z);
        _tr.position = newPosition;
    }
    
    //Set Position Z
    public static void SetZ(this Transform _tr, float value)
    {
        Vector3 newPosition = new Vector3(_tr.position.x, _tr.position.y, value);
        _tr.position = newPosition;
    }
    
    //Set Position X Y Z
    public static void SetPosition(this Transform _tr, float ?x, float ?y, float ?z)
    {
        Vector3 newPosition = new Vector3(x ?? _tr.position.x, y ?? _tr.position.y, z ?? _tr.position.z);
        _tr.position = newPosition;
    }
    
    //Add Child Object
    public static void AddChild(this Transform _tr, GameObject child)
    {
        child.transform.SetParent(_tr);
    }
    
    public static void AddChild(this Transform _tr, Transform child)
    {
        child.SetParent(_tr);
    }

    public static void ClearChildren(this Transform _tr)
    {
        for (int i = _tr.childCount - 1; i >= 0; i--)
        {
            GameObject.DestroyImmediate(_tr.GetChild(i).gameObject);
        }
    }
    
}
