using UnityEngine;
using System.Collections;

public static class ExtGameObject
{
	//Add Child Object
	public static void AddChild (this GameObject _go, GameObject child, bool checkExist = false)
	{
		if (!checkExist)
			child.transform.parent = _go.transform;
		else
		{
			if (_go.transform.Find (child.name) == null)
				child.transform.parent = _go.transform;
		}
		
	}
	
	//Add Child Object
	public static GameObject CreateChild (this GameObject _go, string childName, bool checkExist = false)
	{
		GameObject go;
		if (!checkExist)
		{
			go = new GameObject(childName);
			go.transform.parent = _go.transform;
			return go;
		}
		else
		{
			if (_go.transform.Find (childName) == null)
			{
				go = new GameObject(childName);
				go.transform.parent = _go.transform;
				return go;
			}
		}
		return null;
	}
	
	//Add Child Object
	public static GameObject CreateGameObject (string name, bool checkExist = false)
	{
		GameObject go;
		if (!checkExist)
		{
			go = new GameObject(name);
			return go;
		}
		else
		{
			if (GameObject.Find (name) == null)
			{
				go = new GameObject(name.Substring(name.LastIndexOf("/")+1));
				return go;
			}
		}
		return null;
	}
	
	//Add Child Object
	public static GameObject InstantiateSameName (Object obj)
	{
		GameObject go = (GameObject.Instantiate(obj) as GameObject);
		go.name = go.name.Replace("(Clone)", "");
		return go;
	}

	//Add Child Object with parent
	public static GameObject InstantiateSameName (Object obj, Transform parent)
	{
		GameObject go = (GameObject.Instantiate(obj) as GameObject);
		go.name = go.name.Replace("(Clone)", "");
		go.transform.parent = parent;
		return go;
	}

	
	public static GameObject InstantiateSameNameResources (string path)
	{
		GameObject go = (GameObject.Instantiate( Resources.Load(path)) as GameObject);
		go.name = go.name.Replace("(Clone)", "");
		return go;
	}
	
	public static void SetParentAtPosZero(this GameObject obj, Transform _parent)
	{
		obj.transform.parent = _parent;
		obj.transform.localPosition = Vector3.zero;
		
		
	}
	
	public static void SetParentAtPos(this GameObject obj, Transform _parent, Vector3 offset)
	{
		obj.transform.parent = _parent;
		obj.transform.localPosition = offset;
		
		
	}
	
}
