using UnityEngine;
using System.Collections;

public static class ExtRect
{
	
	public static Rect ShiftX (this Rect _rect, float offset)
	{
		return new Rect(_rect.x + offset,_rect.y,_rect.width,_rect.height);
	}
	
	public static Rect ShiftY (this Rect _rect, float offset)
	{
		return new Rect(_rect.x ,_rect.y + offset,_rect.width,_rect.height);
	}
	
	public static Rect ShiftW (this Rect _rect, float offset)
	{
		return new Rect(_rect.x,_rect.y,_rect.width + offset,_rect.height);
	}
	
	public static Rect ShiftH (this Rect _rect, float offset)
	{
		return new Rect(_rect.x,_rect.y,_rect.width ,_rect.height + offset);
	}
	
	public static Rect ChangeWidth (this Rect _rect, float newWidth)
	{
		return new Rect(_rect.x,_rect.y,newWidth,_rect.height);
	}
	
	public static Rect ChangeHeight (this Rect _rect, float newHeight)
	{
		return new Rect(_rect.x,_rect.y,_rect.width,newHeight);
	}
	
	public static Rect ShiftRect (this Rect _rect, float x, float y, float w, float h)
	{
		return new Rect(_rect.x + x,_rect.y + y,_rect.width + w,_rect.height + h);
	}
}
