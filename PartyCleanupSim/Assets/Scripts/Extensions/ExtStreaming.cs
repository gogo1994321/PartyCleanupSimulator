using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static  class ExtStreaming
{


		public static string GetPathToStreamingAsset (string localPath, bool addFile = true)
		{

#if UNITY_EDITOR
		return  (addFile ? "file://" : "") + Application.dataPath + "/StreamingAssets/" + localPath;
#elif UNITY_WEBPLAYER 
				return  Application.dataPath + "/StreamingAssets/" + localPath;
#elif UNITY_ANDROID 
			return  ((addFile ? "jar:file://" : "") + Application.dataPath  + "!/assets/" + localPath);
#else 
				return  ((addFile ? "file://" : "") + Application.streamingAssetsPath + "/" + localPath);
#endif
		}

	
	
}
