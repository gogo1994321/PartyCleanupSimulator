﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Helper to set layers
/// </summary>
public static class ExtLayers
{
    /// <summary>
    /// Layers list, needs to be a mirror of layers set in unity
    /// </summary>
    public enum LayersEnum
    {
        Default = 0,
        TransparentFx = 1,
        IgnoreRaycast = 2,
        Builtin3 = 3,
        Water = 4,
        UI = 5,
        Builtin6 = 6,
        Builtin7 = 7,
        Custom_Photo = 8,
        Custom_3D = 9,
        Custom_2D = 10,
        Custom_Effects = 11,
        Custom_Debug = 12
    }

    public static int GetLayer( LayersEnum layersDef)
    {
        if (layersDef == null)
            throw new ArgumentNullException("layerDef");

        return (int) layersDef;
    }

    /// <summary>
    /// Build a mask with the list of layers passed in
    /// </summary>
    /// <param name="layersDef"></param>
    /// <returns>Layer integer</returns>
    public static int GetMask(params LayersEnum[] layersDef)
    {
        if (layersDef == null)
            throw new ArgumentNullException("layerDef");
        int num1 = 0;
        foreach (LayersEnum layerEnum in layersDef)
        {
            int num2 = (int)layerEnum;
            if (num2 != -1)
                num1 |= 1 << num2;
        }
        return num1;
    }
}
