using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ExtVector
{
	
	
	//Set Position X
	public static void SetX (this Vector3 v, float value)
	{
		
		v = new Vector3 (value, v.y, v.z);
	}
	
	//Set Position Y
	public static void SetY (this Vector3 v, float value)
	{
		v = new Vector3 (v.x, value, v.z);
	}
	
	//Set Position Z
	public static void SetZ (this Vector3 v, float value)
	{
		v = new Vector3 (v.x, v.y, value);
	}
	
	public static bool V3Equal (this Vector3 a, Vector3 b)
	{
		return Vector3.SqrMagnitude (a - b) < 0.0001;
	}
	
	public static float AngleSigned (Vector3 v1, Vector3 v2, Vector3 n)
	{
		return Mathf.Atan2 (

        Vector3.Dot (n, Vector3.Cross (v1, v2)),

        Vector3.Dot (v1, v2)) * Mathf.Rad2Deg;

	}

}
