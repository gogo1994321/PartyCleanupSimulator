using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using System;

public static class ExtIo  {
	
	
	public static T[] GetAtPath<T> (string path)
	{
		ArrayList al = new ArrayList ();
		string[] fileEntries = Directory.GetFiles (Application.dataPath + "/" + path);
		foreach (string fileName in fileEntries) {
			
			int index = fileName.LastIndexOf ("/");
			string localPath = "Assets/" + path;
			if (index > 0)
				localPath += fileName.Substring (index + 1);
			object t=null;// = Resources.LoadAssetAtPath (localPath, typeof(T));
			if (t != null)
				al.Add (t );
		}
		T[] result = new T[al.Count];
		for (int i = 0; i < al.Count; i++)
			result[i] = (T)al[i];
		return result;
	}
	
	/*
	public static FileInfo GetNewestFile(DirectoryInfo directory) {
	   
			 return directory.GetFiles()
       .Union(directory.GetDirectories().Select(d => GetNewestFile(d)))
       .OrderByDescending(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
       .FirstOrDefault();        
		
			        
		
		
	}
	
	public static FileInfo[] GetNewestFile(DirectoryInfo directory, number) {
		return (collection.Skip(Math.Max(0, collection.Count() - number)).Take(number).ToArray());
	}
	*/
	public static FileInfo[] GetNewestFiles(string path, int number = 1)
	{
#if !UNITY_WEBPLAYER
		 DirectoryInfo directory = new DirectoryInfo(path);
		FileInfo[] collection = directory.GetFiles("*",SearchOption.AllDirectories)
			.OrderBy(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
			.ToArray();
		
		
		return (collection.Skip(Math.Max(0, collection.Count() - number)).Take(number).ToArray());
#else
        // web player does not support IO functions
        return new FileInfo[0];
#endif
	}
}
