using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static  class ExtString{

	public static string FixTo(this string str, float width, string type="label")
	{
	    var widthOfTab = GUI.skin.GetStyle(type).CalcSize(new GUIContent("\t")).x;
		var widthOfSpace = GUI.skin.GetStyle(type).CalcSize(new GUIContent(" ")).x;
		//var widthOfString = GUI.skin.GetStyle(type).CalcSize(new GUIContent(str)).x;
	    return str + new string(' ', (int)((width-widthOfTab)/widthOfSpace)+1) + "\t";
	}
}
