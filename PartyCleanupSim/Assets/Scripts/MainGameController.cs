﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using Timers;

public enum GameState
{
    InProgress,
    Won,
    Lost
};

[RequireComponent(typeof(TimersManager))]
public class MainGameController : MonoBehaviour {

    private GameState gameState = 0;   //0 - game in progress, 1 - game won, 
    [HideInInspector]
    public UnityAction timeOutAction;

    [SerializeField]
    private int pointsNeededToWin;

    [HideInInspector]
    private int currentPoints;

    [SerializeField]
    private float timeUntilEndOfLevel;

    [System.Serializable]
    class Music {
        public AudioClip musicTrack;
        public float timeToPlay;

        public override string ToString()
        {
            return "Track: "+musicTrack+" Time: "+timeToPlay;
        }
    }

    [SerializeField]
    private List<Music> musics;

    int musicIndexToPlay = 0;   //Keeps track of which index of musics should be played next

	// Use this for initialization
	void Start () {
        timeOutAction = timeOut;

        musics.Sort((x,y) => x.timeToPlay.CompareTo(y.timeToPlay)); //Sorts the list in ascending order compared to the timeToPlay (http://stackoverflow.com/questions/3309188/how-to-sort-a-listt-by-a-property-in-the-object)
        List<Timer> musicTimers = new List<Timer>();
        for (int i = 0; i < musics.Count; i++) {
            musicTimers.Add(new Timer(musics[i].timeToPlay, 1, delegate { SwitchMusic(); }));
        }
        TimersManager.AddTimers(this, musicTimers); //Can't use SetTimer since it deletes any timer with the same delegate

        TimersManager.SetTimer(this, timeUntilEndOfLevel, timeOutAction);
	}

    void timeOut() {
        gameState = GameState.Lost;    //In case some other script needs to know
        Debug.Log("End of game");
    }

    void SwitchMusic() {
        AudioSource source = GetComponent<AudioSource>();
        if (source.isPlaying)
        {
            source.Stop();
        }
        source.clip = musics[musicIndexToPlay].musicTrack;
        source.Play();
        musicIndexToPlay++;
    }

    public void IncreaseWinPoints(int pointToAdd) {
        currentPoints += pointToAdd;
        if (currentPoints >= pointsNeededToWin) {
            TimersManager.ClearTimer(timeOutAction);
            gameState = GameState.Won;
        }
    }

    public string getCurrentPoints() {
        return currentPoints.ToString();
    }

    public string getMaxPoints() {
        return pointsNeededToWin.ToString();
    }

    public GameState getCurrentGameState() {
        return gameState;
    }

}
