﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Timers;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour {

    [SerializeField]
    private Text topDisplay;
	[SerializeField]
	private Text endDisplay;

    private MainGameController gameController;

	public UnityStandardAssets.Cameras.FreeLookCam cam;

    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainGameController>();
    }

	// Update is called once per frame
	void Update () {
        string textToDisplayOnTop = "";

        if (gameController.getCurrentGameState() == GameState.Won) {
			endDisplay.text = "Congratulations you threw a huge House Party and managed to destroy all the evidence of your nefarious deeds. Keep on rocking!";
			endDisplay.transform.parent.gameObject.SetActive (true);
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerControl> ().playerFrozen = true;
			cam.enabled = false;
			Cursor.visible = true;
        }
        else if (gameController.getCurrentGameState() == GameState.Lost) {
			endDisplay.text = "You thought you could throw a party and be popular, too bad you failed at cleaning up, got busted, and are now grounded till graduation. \nCollected points: "+ gameController.getCurrentPoints() + "/" + gameController.getMaxPoints();
			endDisplay.transform.parent.gameObject.SetActive (true);
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerControl> ().playerFrozen = true;
			cam.enabled = false;
			Cursor.visible = true;
        }
        else {
            float remainingTime = TimersManager.RemainingTime(gameController.timeOutAction);
            string displayTime;
            if (remainingTime < 10f) {  //We only want to display decimal point numbers if the timer is below 10 seconds
                displayTime = remainingTime.ToString("F2");
            } else {
                displayTime = remainingTime.ToString("F0");
            }
            textToDisplayOnTop = displayTime + " ------- " + gameController.getCurrentPoints() + "/" + gameController.getMaxPoints(); //Display remaining time left and points
        }
        topDisplay.text = textToDisplayOnTop;
	}

	public void RestartLevelButton(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	public void QuitButton(){
		SceneManager.LoadScene ("MainMenu");
	}
}
