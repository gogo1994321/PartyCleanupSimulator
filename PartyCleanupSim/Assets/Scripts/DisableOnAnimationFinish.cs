﻿using UnityEngine;
using System.Collections;

public class DisableOnAnimationFinish : StateMachineBehaviour {
	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.SetActive(false);
	}
}
