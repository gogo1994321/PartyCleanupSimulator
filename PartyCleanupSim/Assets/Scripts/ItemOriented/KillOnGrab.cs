﻿using UnityEngine;
using System.Collections;
using System;
//Kills the item it's added to when the player interacts with it (duh..)
public class KillOnGrab : MonoBehaviour, IInteractable
{
    [SerializeField]
    private int pointsToAddOnGrab;

    private MainGameController gameController;

    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainGameController>();
    }

    public void Interact()
    {
        gameController.IncreaseWinPoints(pointsToAddOnGrab);
        Destroy(gameObject);
    }
}
