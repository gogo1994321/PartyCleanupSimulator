﻿using UnityEngine;
using System.Collections;

public class ToolItem : MonoBehaviour{

    public GameObject eraseObject;

    public virtual void UseTool(bool use) { //Used by PlayerControl. Theory is that you have an erase object on the tool that you can switch on and off. It should have the killSpecificWhenCollided on it.
        if(eraseObject)
            eraseObject.GetComponent<Collider>().enabled = use;
    }
}
