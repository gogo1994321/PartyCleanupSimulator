﻿using UnityEngine;
using System.Collections;

public class PointOnCorrectPositionTrigger : MonoBehaviour {

    [SerializeField]
    [Tooltip("Target Gameobject (e.x. furniture) that should be in this exact position")]
    private GameObject targetGO;

    [SerializeField]
    [Tooltip("How lenient are we with accepting correct position?")]
    private float possibleErrorMarginPos;

    [SerializeField]
    [Tooltip("How lenient are we with accepting correct rotation? Calculated in degrees")]
    private float possibleErrorMarginRot;

    [SerializeField]
    [Tooltip("Should we show a phantom image in game of the target?")]
    private bool displayPhantomImage;

    [SerializeField]
    [Tooltip("What material should the phantom used if displayed?")]
    private Material phantomMaterial;

    [SerializeField]
    private int pointsToAddOnCorrectPos;

    [SerializeField]
    private bool debug;

    private MainGameController gameController;

    private bool inPos; //Flag to check if target is in pos. Prevents point farming

    private bool inPosPrev;

    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainGameController>();
        if (displayPhantomImage && phantomMaterial) {   //Spawn in a phantom of the target GO and make sure its colliders are off and has the correct material
            GameObject phantom = Instantiate(targetGO) as GameObject;
            phantom.SetParentAtPosZero(transform);
            phantom.transform.localRotation = Quaternion.identity;
            phantom.GetComponent<Rigidbody>().isKinematic = true;
            foreach (Collider col in phantom.GetComponentsInChildren<Collider>()) {
                col.enabled = false;
            }
            foreach (Renderer renderer in phantom.GetComponentsInChildren<Renderer>()) {
                Material[] mats = new Material[renderer.materials.Length];
                for (int i = 0; i < renderer.materials.Length; i++) {
                    mats[i] = phantomMaterial;
                }
                renderer.materials = mats;
            }
        }
    }

    // Update is called once per frame
    void Update ()
    {
        inPos = InCorrectPos();
        if (inPos != inPosPrev) //We only add a point if the status actually changed
        {
            AddPoint();
            inPosPrev = inPos;
        }
	}

    bool InCorrectPos() {   //Tests if the target GO is within the correct pos and rotation with the marginal error

        int decider = 0;
        Vector3 pos = targetGO.transform.position;

        if (pos.x > transform.position.x-possibleErrorMarginPos && pos.x < transform.position.x+possibleErrorMarginPos) {
            decider++;
            if (debug)
                Debug.Log("PosX correct");
        }
        if (pos.y > transform.position.y - possibleErrorMarginPos && pos.y < transform.position.y + possibleErrorMarginPos)
        {
            decider++;
            if (debug)
                Debug.Log("PosY correct");
        }
        if (pos.z > transform.position.z - possibleErrorMarginPos && pos.z < transform.position.z + possibleErrorMarginPos)
        {
            decider++;
            if (debug)
                Debug.Log("PosZ correct");
        }

        Vector3 rot = targetGO.transform.rotation.eulerAngles;
        if (rot.x > transform.eulerAngles.x-possibleErrorMarginRot && rot.x < transform.eulerAngles.x+possibleErrorMarginRot) { 
            decider++;
            if (debug)
                Debug.Log("RotX correct");
        }
        if (rot.y > transform.eulerAngles.y - possibleErrorMarginRot && rot.y < transform.eulerAngles.y + possibleErrorMarginRot)
        {
            decider++;
            if (debug)
                Debug.Log("RotY correct");
        }
        if (rot.z > transform.eulerAngles.z - possibleErrorMarginRot && rot.z < transform.eulerAngles.z + possibleErrorMarginRot)
        {
            decider++;
            if (debug)
                Debug.Log("RotZ correct");
        }

        return decider == 6;
    }

    void AddPoint() {
        if (inPos) //Make sure the player can't farm points by repeatedly dragging the object in and out.
        {
            gameController.IncreaseWinPoints(pointsToAddOnCorrectPos);
        }
        else {
            gameController.IncreaseWinPoints(-pointsToAddOnCorrectPos);
        }
    }
}
