﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(AudioSource))]
public class PlaySoundOnInteract : MonoBehaviour, IInteractable
{
    public AudioClip clip;

    public void Interact()
    {
        GetComponent<AudioSource>().PlayOneShot(clip);
    }
}
