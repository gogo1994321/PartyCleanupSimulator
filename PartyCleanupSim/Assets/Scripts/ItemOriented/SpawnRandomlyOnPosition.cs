﻿using UnityEngine;
using System.Collections;

public class SpawnRandomlyOnPosition : MonoBehaviour {

    [SerializeField]
    Transform[] possiblePositions;

	// Use this for initialization
	void Start () {
		if (possiblePositions.Length > 0) {
			Transform pos = possiblePositions [Random.Range (0, possiblePositions.Length)];
			if (pos != null) {
				transform.position = pos.position;
			}
		}
	}
}
