﻿using UnityEngine;
using System.Collections;

public class SpawnRandomlyInsideArea : MonoBehaviour {

    [SerializeField]
    private Vector3 spawnArea;

    [SerializeField]
    private GameObject[] prefabsToSpawn;

    [SerializeField]
    [Tooltip("Should we choose prefab randomly from the list?")]
    private bool chooseRandomly;

    [SerializeField]
    [Tooltip("How many prefabs do we spawn? If 0, we spawn as many as prefabs on the list")]
    private int amountToSpawn;

    [SerializeField]
    private string tagToAdd;

    [SerializeField]
    private bool randomOrientation;

    [SerializeField]
    private Vector3 orientationToUse;

	// Use this for initialization
	void Start () {
        if (amountToSpawn <= 0) amountToSpawn = prefabsToSpawn.Length;

        for (int i = 0; i < amountToSpawn; i++) {
            int k = chooseRandomly ? Random.Range(0, prefabsToSpawn.Length) : i%prefabsToSpawn.Length;    //If random, get a random index, if not, make sure that we don't go out of array
            Vector3 randomSpawnPoint = transform.position + new Vector3(Random.Range(spawnArea.x,-spawnArea.x), Random.Range(spawnArea.y, -spawnArea.y), Random.Range(spawnArea.z, -spawnArea.z));

            //Try to make sure that we don't overlap with anything on spawn
            int maxTries = 100;
            while (Physics.OverlapSphere(randomSpawnPoint,0.5f).Length > 0 && maxTries > 0) {
                randomSpawnPoint = transform.position + new Vector3(Random.Range(spawnArea.x, -spawnArea.x), Random.Range(spawnArea.y, -spawnArea.y), Random.Range(spawnArea.z, -spawnArea.z));
                maxTries--;
            }
            if (maxTries == 0)
                break;

            //Either use a random orientation, or the one in orientationToUse
            Quaternion quat = randomOrientation ? Quaternion.Euler(Random.Range(0, 359f), Random.Range(0, 359f), Random.Range(0, 359f)) : Quaternion.Euler(orientationToUse);

            GameObject GO = Instantiate(prefabsToSpawn[k],randomSpawnPoint,quat) as GameObject;
            if(tagToAdd != "")
                GO.tag = tagToAdd;
        }
	}

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5F);
        Gizmos.DrawCube(transform.position, spawnArea*2);
    }
}
