﻿using UnityEngine;
using System.Collections;
/*
 * Kills all gameobjects that come into contact with this ones collider and have a specific tag
 */

public class KillSpecificWhenCollided : MonoBehaviour {

    enum WhoToKill { 
        Self,
        Other
    };

    [Tooltip("Tag name of all GOs, that should be killed when colliding with this one")]
    public string tagToKillOnContact;

    [SerializeField]
    [Tooltip("Kill self on impact, or the other?")]
    private WhoToKill whoToKill;

    [SerializeField]
    private int pointsToAddOnKill;

    private MainGameController gameController;

    void Start() {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainGameController>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == tagToKillOnContact || tagToKillOnContact == "")
        {
            switch(whoToKill){
                case WhoToKill.Self:
                    Destroy(this.gameObject);
                    break;
                case WhoToKill.Other:
                    Destroy(other.gameObject);
                    break;
            }
            gameController.IncreaseWinPoints(pointsToAddOnKill);
        }
    }
}
