﻿using UnityEngine;
using System.Collections;

public class PullTowardsPointWhenTriggered : MonoBehaviour {

    public Transform pullPoint;
    public float force = 10;
    public string tagToPull = "killable";

    void OnTriggerStay(Collider col) {
		if (col.tag == tagToPull && col.attachedRigidbody!=null) {
            Vector3 forceDir = (pullPoint.position - col.transform.position).normalized * force*Time.deltaTime;
            col.attachedRigidbody.AddForce(forceDir);
            //Debug.Log("Pulling "+col.name);
        }
    }
}
