﻿using UnityEngine;
using UnityStandardAssets.Utility;
using System.Collections;

public class ForceResettel : MonoBehaviour {
    //Used as a killzone for rigidbodies in the level in case they fall through the floor.
    void OnTriggerEnter(Collider col) {
        if (col.GetComponent<ObjectResetter>() != null) {
            Debug.Log("Resetting: "+col.name);
            col.GetComponent<ObjectResetter>().DelayedReset(0f);
        }
    }
}
